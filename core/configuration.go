package core

import (
	"encoding/json"
	"io/ioutil"
	"strings"
)

type Configuration struct {
	// photo path
	PhotoPath string `json:"photoPath"`

	// miniature photo storage path
	CachePath string `json:"cachePath"`

	// TLS configuration
	CertificatePath string `json:"certificatePath"`
	PrivateKeyPath string `json:"privateKeyPath"`

	// key to access to the content
	AccessKey string `json:"accessKey"`

	// hostname
	Hostname string `json:"hostname"`
}

// load configuration
func BuildConfiguration( filePath string ) ( *Configuration, error ) {
	// allocate
	configuration := &Configuration{ }

	// read file content
	if fileContent, err := ioutil.ReadFile( filePath ); err == nil {
		if err = json.Unmarshal( fileContent,
			configuration ); err == nil {
				strings.TrimRight( configuration.PhotoPath,
					"/" )
				configuration.PhotoPath += "/"
				strings.TrimRight( configuration.CachePath,
					"/" )
				configuration.CachePath += "/"
				return configuration, nil
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
}

package main

import (
	"./core"
	"fmt"
	"os"
)

const(
	DefaultConfigurationFilePath = "./configuration.json"
)

func main( ) {
	// configuration file path
	configurationFilePath := DefaultConfigurationFilePath
	if len( os.Args ) > 1 {
		configurationFilePath = os.Args[ 1 ]
	}

	// load configuration
	if configuration, err := core.BuildConfiguration( configurationFilePath ); err == nil {
		// build core
		if c, err := core.BuildCore( configuration ); err == nil {
			// run core
			c.Run()
		}
	} else {
		fmt.Println( err )
	}
}
